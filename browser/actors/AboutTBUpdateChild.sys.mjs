// Copyright (c) 2020, The Tor Project, Inc.
// See LICENSE for licensing information.
//
// vim: set sw=2 sts=2 ts=8 et syntax=javascript:

import { RemotePageChild } from "resource://gre/actors/RemotePageChild.sys.mjs";

export class AboutTBUpdateChild extends RemotePageChild {}
